package main

import (
	"bitbucket.org/bemobidev/log"
	"bitbucket.org/bemobidev/class-control-system-api/settings"
	"github.com/bemobi/envconfig"
	"github.com/gorilla/mux"
	"net/http"
	"os"
	"time"
)

var Logger *log.Context

func init(){
	Logger = &log.Context{Emitter: log.New(os.Stderr, time.RFC3339), Tag: "css-api"}
	err := envconfig.InitWithOptions(&settings.Settings, envconfig.Options{AllOptional:true})


	if err != nil {
		Logger.E("Error on start server.", "err", err)
	}

	Logger.I("Starting server...")
	Logger.I("Success to start the server", "settings", settings.Settings.Database)
}


func main(){

	//Routes
	r := mux.NewRouter()

	//Starting server
	http.Handle("/", r)
	server := &http.Server{
		Addr:           ":5000",
		Handler:        nil,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	//Error on start server
	if err := server.ListenAndServe(); err != nil {

		Logger.E("Error to connect the server")
		panic(err)
	}

}
