module bitbucket.org/bemobidev/class-control-system-api

go 1.12

require (
	bitbucket.org/bemobidev/log v1.4.0
	github.com/bemobi/envconfig v1.0.0
	github.com/gorilla/mux v1.7.3
)
