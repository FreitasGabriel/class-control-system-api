package model

import "time"

type User struct {
	Id           int       `gorm:"column:id" json:"id"`
	Registration int       `gorm:"column:registration" json:"registration"`
	Name         string    `gorm:"column:name" json:"name"`
	Birthday     time.Time `gorm:"column:birthday" json:"birthday"`
	Address      struct {
		Street    string `gorm:"column:street" json:"street"`
		Number    string `gorm:"column:number" json:"number"`
		City      string `gorm:"column:city" json:"city"`
		Reference string `gorm:"column:reference" json:"reference"`
	}
	UserProfile string `gorm:"column:user_profile" json:"user_profile"`
}

type Question struct {
	Id         int    `gorm:"column:id" json:"id"`
	Question   string `gorm:"column:question" json:"question"`
	Answer     string `gorm:"column:answer" json:"answer"`
	CreatedFor User   `gorm:"column:created_for" json:"created_for"`
}

type Test struct {
	Id       int `gorm:"column:id" json:"id"`
	Question map[string]Question
}

type Topic struct {
	Id          int                `gorm:"column:id" json:"id"`
	Title       string             `gorm:"column:title" json:"title"`
	Description string             `gorm:"column:description" json:"description"`
	Comments    map[string]Comment `gorm:"column:comments" json:"comments"`
	CreatedAt   time.Time          `gorm:"column:created_at" json:"created_at"`
	CreatedFor  User               `gorm:"column:created_for" json:"created_for"`
}

type Comment struct {
	Id      int    `gorm:"column:id" json:"id"`
	Content string `gorm:"column:content" json:"content"`
	User    User   `gorm:"column:user" json:"user"`
}

type Material struct {
	Id      int       `gorm:"column:id" json:"id"`
	Name    string    `gorm:"column:name" json:"name"`
	SendAt  time.Time `gorm:"column:send_at" json:"send_at"`
	SendFor User      `gorm:"column:send_for" json:"send_for"`
}
