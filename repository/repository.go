package repository

import (
	"bitbucket.org/bemobidev/log"
	"bitbucket.org/bemobidev/class-control-system-api/settings"
	"fmt"
	"github.com/jinzhu/gorm"
	"net/url"
)

type DB struct {
	Logger *log.Context
	Db *gorm.DB
}

//COMMENT








func (d *DB) MustInit() {
	var err error
	d.Db, err = gorm.Open("postgres", connectionDB)
	if err != nil {
		d.Logger.E("Error to connect database", "err", err)
		return
	}

	d.Db.DB().SetMaxOpenConns(settings.Settings.Database.SetMaxOpenConns)
	d.Db.LogMode(true) //habilita o modo de log

}

func connectionDB() string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s:%s/%s?sslmode=disable",
		settings.Settings.Database.UserName,
		url.QueryEscape(settings.Settings.Database.Password),
		settings.Settings.Database.Host,
		settings.Settings.Database.Port,
		settings.Settings.Database.DatabaseName,

	)

}