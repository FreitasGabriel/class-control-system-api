package settings

var Settings struct {
	Database struct {
		UserName        string `envconfig:"default=postgres"`
		Password        string `envconfig:"default=2318"`
		Host            string `envconfig:"default=localhost"`
		Port            string `envconfig:"default=5432"`
		DatabaseName    string `envconfig:"default=seu-orlando-burguer-db"`
		SetMaxOpenConns int    `envconfig:"default=100"`
	}
}
